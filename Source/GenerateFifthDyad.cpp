/*
 ==============================================================================
 
 GenerateFifthDyad.cpp
 Created: 21 Dec 2016 12:51:55pm
 Author:  Thomas Harvey
 
 ==============================================================================
 */

#include "GenerateFifthDyad.h"

GenerateDyad::GenerateDyad()
{
    
    
}
GenerateDyad::~GenerateDyad()
{
    
    
    
}

void GenerateDyad::calculateDyad(int currentNote_ , int chord_, int dyadType_, bool tooHigh, bool tooLow, int randNum)
{
    
    // Which dyads contain this note?
    // Out of the two, which is the most popular on a minor chord?
    
    
    int dyad1[2] = {0}; // first element is which dyad it is 0-6
    int dyad2[2] = {0}; // second element is : 0 = 1st , 1 = 3rd
    bool checkDyad = false;
    int dyadChoice = 0;
    int length = 0;

    
    
    // fithdyad[7][2]
    
    if (randNum == 2 || randNum == 28 || randNum == 3 || randNum == 29 || randNum == 52|| randNum == 53)
    {
        dyadArrayPointer = fifthDyad;   // point to a fith 2d array
    }
    
    else if (randNum == 0 || randNum == 26 || randNum == 1 || randNum == 27 || randNum == 50|| randNum == 51 == 1)
    {
        dyadArrayPointer = thirdDyad;   // point to the third 2d array
    }
    
    dyadArray.add(currentNote_);
    
    
    for (int count = 0; count < 7; count ++)
    {
        for (int counter = 0 ; counter < 2; counter ++)
        {
            
            if ((currentNote_ - dyadArrayPointer[count][counter] ) % 12 == 0)
            {
                
                if(checkDyad == false)  // If its not found a dyad
                {
                    dyad1[0] = count;       // set the dyad number 0 -6 for each note
                    dyad1[1] = counter;     // set the position: 1st or 3rd/5th
                    checkDyad = true;
                }
                
                
                else if(checkDyad == true)   // If it has found a dyad
                {
                    dyad2[0] = count;
                    dyad2[1] = counter;
                    checkDyad = true;
                }
            }
            
        }
        
    }
    
    // randomly chose which dyad baed on which is more popular on the selected chord
    
    for (int count = 0 ; count < 12 ; count ++)
    {
        
    }
    
    
    if (tooLow == true)
    {
        setDyad(dyad2, currentNote_);    // with current note as root
    }
    
    else if (tooHigh == true)
    {
        setDyad(dyad1, currentNote_);    // with current note as third
    }
    
    
    else if (randNum == 0 || randNum == 1|| randNum == 51 || randNum == 2 || randNum == 3 || randNum == 52)
    {
        setDyad(dyad2, currentNote_);    // with current note as root
    }
    else if (randNum == 26|| randNum == 27 || randNum == 28 || randNum == 29 || randNum == 51 || randNum == 53)
    {
        setDyad(dyad1, currentNote_);    // with current note as root
    }
 

    
    lastNote = dyadArray[dyadArray.size() - 1];
    
}

void GenerateDyad::setDyad(int dyad_[], int currentNote_)
{
    int nextNote = 0;
    int i = 0;
    
    if (dyad_[0] == 5 || dyad_[1] == 6) // if the triad numver is 5 or 6, adds 12 to the third to make it bigger than the first
    {
        i = 12;
    }
    
    if (dyad_[1] == 0) // If this is the root, accend
    {
        nextNote += (dyadArrayPointer[dyad_[0]][1] + i)- dyadArrayPointer[dyad_[0]][0];   // This works out the third - the first to work out if its major or minor
        nextNote += currentNote_;
    }
    
    else if (dyad_[1] == 1) // If this is the third, decend
    {
        nextNote += (dyadArrayPointer[dyad_[0]][0] + i)- dyadArrayPointer[dyad_[0]][1];
        nextNote += currentNote_;
    }
    
    
    
    dyadArray.add(nextNote);
    
}
