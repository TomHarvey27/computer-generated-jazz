/*
  ==============================================================================

    GenerateSequence.cpp
    Created: 23 Feb 2017 8:31:19am
    Author:  Thomas Harvey

  ==============================================================================
*/

#include "GenerateSequence.h"

#include <stdio.h>
GenerateSequence::GenerateSequence()
{
    
    melodicDeviceMinorProbabilities.resize(12);
    melodicDeviceDominantProbabilities.resize(12);
    melodicDeviceMajorProbabilities.resize(12);


    for (int count = 0; count < 20; count ++)
    {
        
        for (int counter = 0 ; counter < rhythm1Probabilities[count]; counter ++)
        {
            rhythm1Values.add(count);
        }
        
    }
    
    std::cout << rhythm1Values.size();
    
}


GenerateSequence::~GenerateSequence()
{
    

    
}

void GenerateSequence::generateRhythm(int totalRhythm, Array<int> chordDuration_, int songRepeat)
{
    
    int rhythmTooLong = 1;
    int currentTimestamp = 0 - chordDuration_[0];
    int counter = 0;
    
    
    for (int count = 0; count < chordDuration_.size() * songRepeat; count ++) // creates a new array with timestamps at each chord change
    {
        
        if (counter == chordDuration_.size())
        {
            counter = 0;
        }
        
        currentTimestamp = chordDuration_[counter] + currentTimestamp;
        std::cout << "CHORDS" << currentTimestamp << std::endl;
        chordTimeStamps.add(currentTimestamp);
        
        counter ++;
    }
    
    
    while (rhythmCounter < totalRhythm )
    {
        
        if (longRhythmCounter == 0)
        {
            randNum = random.nextInt(340) + 1;
            rhythms = rhythmLibrary.returnRhythm(rhythm1Values[randNum]); // figure out random ranges
        }
        
        if (rhythms.longRest == 1 && longRhythmCounter == 0) // if its a long rhythm
        {
            longRhythmCounter = 1;
        }
        
       else if ( longRhythmCounter > 0)
       {
           longRhythmCounter ++;
           
           while (rhythmTooLong == 1)
           {
               randNum = random.nextInt(340) + 1;
               rhythms = rhythmLibrary.returnRhythm(rhythm1Values[randNum]); // figure out random ranges
               rhythmTooLong = rhythms.longRest;
           }
           
           if (longRhythmCounter == 4)
           {
               longRhythmCounter = 0;
           }
           
           rhythmTooLong = 1;
       }
        
      //  rhythms = rhythmLibrary.returnRhythm(2); // Generates specific rhythm for testing
        std::cout << "Length = " << rhythms.length << std::endl;
        
        for (int count = 0; count < rhythms.length; count ++)
        {
            sequenceRhythm.noteOn.add(rhythms.noteOn[count] + previousNoteOff);
            sequenceRhythm.noteOff.add(rhythms.noteOff[count] + previousNoteOff);
            sequenceRhythm.rest.add(rhythms.rest[count]);
            
            if (sequenceRhythm.noteOn[sequenceRhythm.noteOn.size() - 1] >= chordTimeStamps[chordCounter] ) // if the current note on is bigger or equal to the next chord change
            {
                sequenceRhythm.firstNoteOfChord.add(1); // mark this as the first note for that chord
                chordCounter ++;
            }
            
            else
            {
            sequenceRhythm.firstNoteOfChord.add(0); // if its not the new chord mark it as 0
            }
 
        }
        
        rhythmCounter += rhythms.noteOff[rhythms.noteOff.size() - 1]; // increment the counter by the total length of the rhythms
        previousNoteOff += rhythms.noteOff[rhythms.noteOff.size() - 1]; // this is the starting time for the next rhythm copied to the array
    }
    
    rhythmCounter = 0;
//    
    for (int count = 0; count < sequenceRhythm.noteOn.size(); count ++)
    {
        std::cout << "Note on  = " << sequenceRhythm.noteOn[count] << std::endl;
        std::cout << "Note off  = " << sequenceRhythm.noteOff[count] << std::endl;
        std::cout << "Rest  = " << sequenceRhythm.rest[count] << std::endl;
    }
    
   
}


void GenerateSequence::setNotesInSequence(int chordNumber_, int chordDuration_, int key_, int nextChord_, int nextKey_)
{
    
    key = key_;
    chord = chordNumber_;
    nextChord = nextChord_;
    chordDuration = chordDuration_;
    nextKey = nextKey_;
   // previousNote = previousNote + key;
    
    std::cout << "Chord number = " << chord << std::endl;
    
    firstTime = true;
    while (sequenceDuration < chordDuration)    // It exits when one bar has been played
    {

        if (sequenceRhythm.rest[sequenceCounter] == 1) // if its not a rest
        {
            setDirection();
            setCurrentNote();
        }
        
        else    // fix this only temporary
        {
            sequenceRhythm.noteNumber.add(0);
            testCoutner ++;
        }
       
        sequenceDuration = sequenceRhythm.noteOff[sequenceCounter]; // make this a coutner
        sequenceCounter ++;

    }
    
    //firstTime = true;
}

void GenerateSequence::setCurrentNote()
{
 
    // WITHIN THE SPACE OF 8 NOTES IT CAN'T PLAY THE SAME INTERVAL FROM THE SAME NOTES MORE THAN THREE TIMES, AND IT CAN'T IN  A ROW EITHER
    
    int noteValue = 0;;
   // std::cout << "previous note = " << previousNote << std::endl;
    
    for (int count = 0; count < 12; count ++)
    {
        if (((previousNote - key) - count) % 12 == 0 )
        {
            noteValue = count;
        }
    }
    
    
    if (firstTime == true) // if its the first note on a chord
    {
        findNearestChordTone(chord, key);
        firstTime = false;

    }
    
    else    // if its already on a chord
    {
        
        
       // if there are no notes in the right direction, find nearest chord tone
        
        
        if (chord == 1)
        {
            
            seed = sumOfMelodicDeiveMinor[noteValue];
            if (seed <= 0)
            {
                findNearestChordTone(chord, key);
                firstTime = false;
              
            }
            
            else
            {
                
                randNum = random.nextInt(seed); // how many probabilites are avaialble for the specific tone // figure out random messing up and see if they are the correct size
                randNum = melodicDeviceMinorProbabilities[noteValue][randNum];
                
             
                if (direction == -1 && randNum > 11) // if its too high
                {
                    while (randNum > 11)
                    {
                        randNum = random.nextInt(seed); // how many probabilites are avaialble for the specific tone
                        randNum = melodicDeviceMinorProbabilities[noteValue][randNum];

                    }
                   
                }
               
               else if (direction == 1 && randNum < 12) // if its too high
                {
                    while (randNum < 12)
                    {
                        randNum = random.nextInt(seed); // how many probabilites are avaialble for the specific tone
                        randNum = melodicDeviceMinorProbabilities[noteValue][randNum];
                    }
                    
                }
                
               addInterval(randNum);
               //  findNearestChordTone();
                
            }
            
        }
        
     
       else if (chord == 4)
        {
            
            seed = sumOfMelodicDeiveDominant[noteValue] - 1;
            if (seed <= 0)
            {
                findNearestChordTone(chord, key);
                firstTime = false;
                
            }
            
            else
            {
            randNum = random.nextInt(seed); // how many probabilites are avaialble for the specific tone
            randNum = melodicDeviceDominantProbabilities[noteValue][randNum];
                
                
                
                if (direction == -1 && randNum > 11) // if its too high
                {
                    while (randNum > 11)
                    {
                        randNum = random.nextInt(seed); // how many probabilites are avaialble for the specific tone
                        randNum = melodicDeviceDominantProbabilities[noteValue][randNum];
                    }
                    
                }
                
                else if (direction == 1 && randNum < 12) // if its too high
                {
                    while (randNum < 12)
                    {
                        randNum = random.nextInt(seed); // how many probabilites are avaialble for the specific tone
                        randNum = melodicDeviceDominantProbabilities[noteValue][randNum];
                    }
                    
                }
                
            addInterval(randNum);
                //findNearestChordTone();
            }
        }
 
        
       else if (chord == 0)
        {
            if (seed <= 0)
            {
                findNearestChordTone(chord, key);
                firstTime = false;
                
            }
            
            else
            {
            seed = sumOfMelodicDeiveMajor[noteValue] - 1;
            randNum = random.nextInt(seed); // how many probabilites are avaialble for the specific tone
            randNum = melodicDeviceMajorProbabilities[noteValue][randNum];
                
                
                
                if (direction == -1 && randNum > 11) // if its too high
                {
                    while (randNum > 11)
                    {
                        randNum = random.nextInt(seed); // how many probabilites are avaialble for the specific tone
                        randNum = melodicDeviceMajorProbabilities[noteValue][randNum];
                    }
                    
                }
                
                else if (direction == 1 && randNum < 12) // if its too high
                {
                    while (randNum < 12)
                    {
                        randNum = random.nextInt(seed); // how many probabilites are avaialble for the specific tone
                        randNum = melodicDeviceMajorProbabilities[noteValue][randNum];
                    }
                    
                }
                
            addInterval(randNum);
            //findNearestChordTone();
            }
            
        } // 
        
        
        // generate interval and add it to the array
    }
    
//    sequenceDuration +=240; // This will count through an array and add each element to it
//    sequenceCounter ++;

    
}


void GenerateSequence::findNearestChordTone(int chordNumber_, int key_)
{
    int octave = 0;
    int testNote = 0;
    int increment = 10;
    int previousIncrement = 10;
    int currentIncrement = 0;
    int finalIncrement = 0;
    int testNotes[5] = {0};
    bool incrementIsNegative[5];
    

    
  //  std::cout << "previous note = " << previousNote << std::endl;
 
        
        for (int count = 0; count < 12 ; count ++)
        {
            if(((previousNote - key_) - count ) % 12 == 0)
            {
                octave = (previousNote - count) / 12; // works out the octave
            }
        }
        
        for (int count = 0; count < 5; count ++)
        {
            incrementIsNegative[count] = false;                         // test difference between adding and taking key away
            testNote = (octave * 12)  + chordTones[chordNumber_][count] + key_; // makes the test note in the same octave
            testNotes[count] = previousNote - testNote;
            testNotes[count] = testNote;
            //std::cout << "Test notes = " << testNotes[count] << std::endl;
            
            
            increment = previousNote - testNotes[count];
            currentIncrement = increment;
            
            if (currentIncrement < 0) // if its bigger than times it by - 1
            {
                currentIncrement *= -1;
                incrementIsNegative[count] = false;
            }
            
            else    // if its smaller
            {
                incrementIsNegative[count] = true;
            }
            
            
            if (currentIncrement < previousIncrement && currentIncrement !=0)
            {
                
                previousIncrement = currentIncrement;
                if (incrementIsNegative[count] == true)
                {
                    finalIncrement = previousIncrement * -1;
                }
                
                else
                {
                    finalIncrement = previousIncrement;
                }
            }
            
        }
    

    //chordCounter
    
         previousNote = previousNote + finalIncrement;
         sequenceRhythm.noteNumber.add(previousNote);
    testCoutner ++;
    
}

void GenerateSequence::addInterval(int intervalNumber)
{
    int note;
    

    if (intervalNumber == 0)   // if it is the same
    {
        note = 0;
    }
   
   else if (intervalNumber == 1)
    {
        note = -1;
    }
 
   else if (intervalNumber == 2)
   {
       note = -2;
   }
   else if (intervalNumber == 3)
   {
       note = -3;
   }
   else if (intervalNumber == 4)
   {
       note = -4;
   }
    
   else if (intervalNumber == 5)
   {
       note = -5;
   }
    
   else if (intervalNumber == 6)
   {
       note = -6;
   }
    
   else if (intervalNumber == 7)
   {
       note = -7;
   }
    
   else if (intervalNumber == 8)
   {
       note = -8;
   }
    
   else if (intervalNumber == 9)
   {
       note = -9;
   }
    
   else if (intervalNumber == 10)
   {
       note = -10;
   }
    
   else if (intervalNumber == 11)
   {
       note = -11;
   }
    
   else if (intervalNumber == 12)
   {
       note = 12;
   }
    
   else if (intervalNumber == 13)
   {
       note = 1;
   }
    
   else if (intervalNumber == 14)
   {
       note = 2;
   }
    
   else if (intervalNumber == 15)
   {
       note = 3;
   }
    
   else if (intervalNumber == 16)
   {
       note = 4;
   }
    
   else if (intervalNumber == 17)
   {
       note = 5;
   }
    
   else if (intervalNumber == 18)
   {
       note = 6;
   }
    
   else if (intervalNumber == 19)
   {
       note = 7;
   }
    
   else if (intervalNumber == 20)
   {
       note = 8;
   }
    
   else if (intervalNumber == 21)
   {
       note = 9;
   }
    
   else if (intervalNumber == 22)
   {
       note = 10;
   }
    
   else if (intervalNumber == 23)
   {
       note = 11;
   }
    
    

    bool isNoteChromatic = true;
    bool foundNearestChordTone = false;
    
    for (int count = 0; count < 7 ; count ++)
    {
        if((((previousNote + note) - key) - diatonicNotes[count] ) % 12 == 0)
        {
            isNoteChromatic = false; // Sets the note to be not chromatic
        }
    }
    
    
    
   
    
    std::cout << "REST = " << sequenceRhythm.rest[sequenceRhythm.noteNumber.size() + 1] << std::endl;
    if (sequenceRhythm.rest[sequenceRhythm.noteNumber.size() + 1] == 0) // if the note after is a rest
    {

        if(sequenceRhythm.noteOff[sequenceRhythm.noteNumber.size()] -
           sequenceRhythm.noteOn[sequenceRhythm.noteNumber.size()] < 480 &&
           sequenceRhythm.firstNoteOfChord[sequenceRhythm.noteNumber.size() + 1] == 1 ) // If the current note is smaller than a crotchet and the next note is the first note of a chord
        {
            findNearestChordTone(nextChord, nextKey); // find the nearest chord tone of the next chord if
            foundNearestChordTone = true;
        }
        
        else// if the next note is just a rest, end on a chord tone
        {
            findNearestChordTone(chord, key);
            foundNearestChordTone = true;
        }
        
        if (chromaticCounter < 0)
        {
            chromaticCounter ++;
        }
        
        if (chromaticCounter == 4)
        {
            chromaticCounter = 0;
        }
        
        
    }
    
  else  if (isNoteChromatic == true && chromaticCounter == 0) // if the note is chromatic and there hasn't been one in the space of four notes
    {
        chromaticCounter = 1; // start the counter
        
        std::cout << "REST" << sequenceRhythm.rest[sequenceRhythm.noteNumber.size() + 1] << std::endl;
        

        
         if(sequenceRhythm.noteOff[sequenceRhythm.noteNumber.size() + 1] -
                sequenceRhythm.noteOn[sequenceRhythm.noteNumber.size() + 1] > 240) // if the note is bigger than a quaver, make it in key
        {
            if (note < 1)       // makes it in key because every note is one note away from being in key
            {
                note = note - 1;
            }
            
            else if (note > 0)
            {
                note = note + 1;
            }
            
        }
 

    }
    
    else if (chromaticCounter > 0)  // if the counter is bigger that 0
    {
        chromaticCounter ++;
        
        
        if (isNoteChromatic == true)
        {
            if (note < 1)       // makes it in key because every note is one note away from being in key
            {
                note = note - 1;
            }
            
            else if (note > 0)
            {
                note = note + 1;
            }
        }
        
        
        if (chromaticCounter == 4)
        {
            chromaticCounter = 0; // RESET THE COUNTER AFTER FOUR TIMES
        }
        
    }

    

    
    if (foundNearestChordTone == false)
    {
        sequenceRhythm.noteNumber.add(note + previousNote);
        previousNote = note + previousNote;
        testCoutner ++;
        
    }

}

void GenerateSequence::setDirection()
{
    
    if (previousNote < 60)
    {
        direction = 1;
    }
    
    else if (previousNote > 84)
    {
        direction = -1;
    }
    
    else
    {
        direction = 0;
    }
}



void GenerateSequence::setChordProbabilities(int melodicDevicePerChord[12][24], int chord)
{
    
    // use a vector pointer
    
    
    
    if ( chord == 0)
    {
        
        for (int counter = 0; counter < 12; counter ++)
        {
            
            for (int count = 0; count < 24; count ++)
            {
                
                if (melodicDevicePerChord [counter][count] != 0)
                {
                    
                    sumOfMelodicDeiveMinor[counter] += melodicDevicePerChord[counter][count];
                   // std::cout << "MIN = " << counter << " " << sumOfMelodicDeiveMinor[counter] << " = " << melodicDevicePerChord[counter][count] << std::endl;
                }
                
            }
            
        }
        
        for (int count = 0; count < 12; count ++)
        {
            melodicDeviceMinorProbabilities[count].resize(sumOfMelodicDeiveMinor[count]);
        }
        
        minorVectorCounter = 1;
        
        for (int counter = 0; counter < 12; counter ++)
        {
           // std::cout << "Minor Note = " << counter  << std::endl;
            
            for (int count = 0; count < 24; count ++)
            {
                
                if(melodicDevicePerChord[counter][count] != 0  && sumOfMelodicDeiveMinor[counter] != 0)
                {
                    for (int count2 = 1; count2 <= melodicDevicePerChord[counter][count] ; count2 ++)
                    {
                        melodicDeviceMinorProbabilities[counter][minorVectorCounter] = count;
                       // std::cout << minorVectorCounter << ": Chord probabilities MINOR = " << melodicDeviceMinorProbabilities[counter][minorVectorCounter] << std::endl;
                        minorVectorCounter ++;
                    }
                }
                
                
            }
            
            minorVectorCounter = 0;
        }
        
    }
    
    
    if ( chord == 1)
    {
        
        for (int counter = 0; counter < 12; counter ++)
        {
            
            for (int count = 0; count < 24; count ++)
            {
                
                if (melodicDevicePerChord [counter][count] != 0)
                {
                    sumOfMelodicDeiveDominant[counter] += melodicDevicePerChord[counter][count];
                   // std::cout << "DOM = " << counter << " " <<  sumOfMelodicDeiveDominant[counter] << " = " << melodicDevicePerChord[counter][count] << std::endl;
                    
                }
                
            }
            
        }
        
        for (int count = 0; count < 12; count ++)
        {
            melodicDeviceDominantProbabilities[count].resize(sumOfMelodicDeiveDominant[count]);
        }
        
        minorVectorCounter = 0;
        
        for (int counter = 0; counter < 12; counter ++)
        {
           // std::cout << "Chord = " << counter  << std::endl;
            
            for (int count = 0; count < 24; count ++)
            {
                
                if(melodicDevicePerChord[counter][count] != 0  && sumOfMelodicDeiveDominant[counter] != 0)
                {
                    for (int count2 = 1; count2 <= melodicDevicePerChord[counter][count] ; count2 ++)
                    {
                        melodicDeviceDominantProbabilities[counter][minorVectorCounter] = count;
                       // std::cout << minorVectorCounter << ": Chord probabilities Dominant = " << melodicDeviceDominantProbabilities[counter][minorVectorCounter] << std::endl;
                        minorVectorCounter ++;
                    }
                }
                
                
            }
            
            minorVectorCounter = 0;
        }
        
    }
    
    
    if (chord == 2)
    {
        
        
        for (int counter = 0; counter < 12; counter ++)
        {
            
            for (int count = 0; count < 24; count ++)
            {
                
                if (melodicDevicePerChord [counter][count] != 0)
                {
                    sumOfMelodicDeiveMajor[counter] += melodicDevicePerChord[counter][count];
                   // std::cout << "MAj = " << counter << " " << sumOfMelodicDeiveMajor[counter] << " = " << melodicDevicePerChord[counter][count] << std::endl;
                }
            }
            
        }
        
        for (int count = 0; count < 12; count ++)
        {
            melodicDeviceMajorProbabilities[count].resize(sumOfMelodicDeiveMajor[count]);
        }
        
        minorVectorCounter = 0;
        
        for (int counter = 0; counter < 12; counter ++)
        {
           // std::cout << "Chord = " << counter  << std::endl;
            
            for (int count = 0; count < 24; count ++)
            {
                
                if(melodicDevicePerChord[counter][count] != 0  && sumOfMelodicDeiveMajor[counter] != 0)
                {
                    for (int count2 = 1; count2 <= melodicDevicePerChord[counter][count] ; count2 ++)
                    {
                        melodicDeviceMajorProbabilities[counter][minorVectorCounter] = count;
                      //  std::cout << minorVectorCounter << ": Chord probabilities MAjor = " << melodicDeviceMajorProbabilities[counter][minorVectorCounter] << std::endl;
                        minorVectorCounter ++;
                    }
                }
                
                
            }
            
            minorVectorCounter = 0;
        }
        
    }
    
    
    minorVectorCounter = 0;
    
}

