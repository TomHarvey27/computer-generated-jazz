/*
 ==============================================================================
 
 This file was auto-generated!
 
 It contains the basic startup code for a Juce application.
 
 ==============================================================================
 */

#include "../JuceLibraryCode/JuceHeader.h"
#include "CalculateSequence.h"
#include "GenerateSequence.h"
#include "ReadFiles.h"
#include "ChordLibrary.h"
#include "RhythmLibrary.h"
#include <vector>
using std::vector;

//==============================================================================

int main (int argc, char* argv[])
{
    
//    Random rand;
//    
//    int num[8] = {0};
//    
//    for (int count = 0; count < 8; count ++)
//    {
//        num[count] = rand.nextInt(3);
//        std::cout << num[count] << std::endl;
//    }
// 
    
    
    
    ReadFiles readfiles;
    GenerateSequence sequence;
    ChordLibrary chordLibrary;
    ChordLibrary::SongProperties chords;
    
    int songChoice = 2;
    int songRepeat = 8; // set how many times it goes throug the song
    int totalRhythm = 0;
    int chordCounter = 0; // keeps track of what type of chord, 0 = min 1 = dominant 2 = maj
    
    while (chordCounter < 3)    // counts through each chord // change back to 3
    {
        readfiles.readfile(chordCounter);
        sequence.setChordProbabilities(readfiles.sequence.melodicDevicePerChord, chordCounter);
        readfiles.resetCounters();

        chordCounter ++;
        
    } // end of chord coutner while loop
    

    /////////////////// GENERATE SEQUENCE \\\\\\\\\\\\\\\\\\\\\\\\\\\
    
    chords = chordLibrary.returnChords(songChoice); // chose the song
    totalRhythm =  songRepeat * chords.songLength; // sets the length of the sequence
    
    // pass in all the chord lengths
    // then when they're in add them to each other to create the actual timestamp
    
    sequence.generateRhythm(totalRhythm, chords.chordDuration, songRepeat);   // generates the rhythm information for the entire sequence
    
    

    float currentTime = 0;
    int count = 0;
    int previousChordDuration = 0;
    int increment = 0;
    
    Random random;

    
    for (int counter = 0; counter < songRepeat; counter ++) // 8 is how many times it goes through the chords
    {
    
        while (currentTime < chords.songLength) // Counts round for as long as the duration of the song
        {
           // std::cout << chords.chordDuration[count] << std::endl;
            
            if (count + 1 == chords.songLength) // if its the last chord
            {
                increment = (chords.songLength - 1) * -1;
            }
            
            else
            {
                increment = 1;
            }
            
            //SET NOTES IN SEQUENCE ADD ANOTHER ARGUMENT, CHORD 2 WHICH IS CHORD NUMBER[COUNT + VARIABLE]
            //VARIABLE WILL ALWAYS BE ONE, UNLESS IF ITS ONE AWAY FROM THE SONG LENGTH THEN VARIABLE WILL EQUAL (-SONGLENGTH)
            
            sequence.setNotesInSequence(chords.chordNumber[count], chords.chordDuration[count] + previousChordDuration,chords.key[count], chords.chordNumber[count + increment],chords.key[count + increment]);
            currentTime += chords.chordDuration[count];
            previousChordDuration += chords.chordDuration[count]; // updates the chord duration 
            count ++;
        }
        
        currentTime = 0;
        count = 0;
    }

   
    
   //Encapsulate print notes
    
    std::cout << "SIZE = " << sequence.sequenceRhythm.noteNumber.size() << std::endl;

    for (int count = 0; count < sequence.sequenceRhythm.noteNumber.size(); count ++)
    {
        std::cout << sequence.sequenceRhythm.noteNumber[count] << std::endl;
    }
    
    
    
    // Encapsulate this to a printing file.
    
    MidiFile mf;                       // create the midi file
    mf.setTicksPerQuarterNote(480);     // essentially the bpm? experiment  //CHANGE BACK TO 480 IF DOESN'T WORK
    
    String str = File::getCurrentWorkingDirectory().getFullPathName();  // where the file will go
    str += String("/LADYBIRDD1") + String(".mid"); // what it will be called
    std::cout << str << "\n";               // print to the console
    File final(str);                            // put everything in str into the file
    
    count = 0;
    MidiMessageSequence midiSequence;   // creates sequence

    
    int sequenceCount = 0;
    count = 0;

        while (sequenceCount < sequence.sequenceRhythm.noteOff[sequence.sequenceRhythm.noteOff.size()-1]) // Adds each note to the sequence
        {
    
            if (sequence.sequenceRhythm.noteNumber[count] != 0) // 0 is a rest, play notes if not a rest
            {
         
            MidiMessage messageOn (0x90 ,sequence.sequenceRhythm.noteNumber[count], 127);  // note on, note number, velocity
            messageOn.setTimeStamp(sequence.sequenceRhythm.noteOn[count]);              // the time which it will play
            MidiMessage messageOff (0x80, sequence.sequenceRhythm.noteNumber[count], 0);   // note off, note number, velocity
            messageOff.setTimeStamp(sequence.sequenceRhythm.noteOff[count]);    // The time it will stop

            midiSequence.addEvent(messageOn);
            midiSequence.addEvent(messageOff);
                
            }
            
            sequenceCount = sequence.sequenceRhythm.noteOff[count]; // increment counter by the value of the rest or note
            count ++;
            
        }
    
    
    
    mf.addTrack(midiSequence);                  // adds the sequence to the file
    
    FileOutputStream fOutputStream (final);     // the output streem is whats in the string
    jassert(!fOutputStream.failedToOpen());
    mf.writeTo(fOutputStream);                  //Write the sequence to the output
    

    return 0;
    }

