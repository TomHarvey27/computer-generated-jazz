/*
  ==============================================================================

    CalculateInterval.cpp
    Created: 22 Feb 2017 6:48:32pm
    Author:  Thomas Harvey

  ==============================================================================
*/

#include "CalculateInterval.h"


CalculateInterval::CalculateInterval()
{
    
}

CalculateInterval::~CalculateInterval()
{
    
}

int CalculateInterval::calculateInterval(Array<int> sequenceNotes, int noteCounter)
{

        calculateMelodicDeviceNumber(sequenceNotes, noteCounter);
        return melodicDeviceNumber;

    
}
int CalculateInterval::calculateMelodicDeviceNumber(Array<int> sequenceNotes, int noteCounter)
{
    
    
    if (sequenceNotes[noteCounter] == sequenceNotes[noteCounter + 1] )   // if it is the same
    {
        melodicDeviceNumber = 0; 
    }
    
    else  if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == 1)   // b2nd
    {
        melodicDeviceNumber = 1;
    }
    
    else  if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == 2)   // 2nd
    {
        melodicDeviceNumber = 2;
    }
 
    else  if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == 3)   // b3rd
    {
        melodicDeviceNumber = 3;
    }
    
    else  if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == 4)   // 3rd
    {
        melodicDeviceNumber = 4;
    }
    
    else  if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == 5)   // 4th
    {
        melodicDeviceNumber = 5;
    }
    
    else  if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == 6)   // #4
    {
        melodicDeviceNumber = 6;
    }
    
    else  if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == 7)   //5th
    {
        melodicDeviceNumber = 7;
    }
    
    else  if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == 8)   // #5th
    {
        melodicDeviceNumber = 8;
    }
    
    else  if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == 9)   // 6th
    {
        melodicDeviceNumber = 9;
    }
    
    else  if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == 10)   // b7th
    {
        melodicDeviceNumber = 10;
    }
    
    else  if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == 11)   // 7th
    {
        melodicDeviceNumber = 11;
    }
    
    else  if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == 12)   // octave
    {
        melodicDeviceNumber = 12;
    }
    
    else  if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == -1)   // b2nd
    {
        melodicDeviceNumber = 13;
    }
    
    else  if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == -2)   // 2nd
    {
        melodicDeviceNumber = 14;
    }
    
    else  if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == -3)   // b3rd
    {
        melodicDeviceNumber = 15;
    }
    
    else  if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == -4)   // 3rd
    {
        melodicDeviceNumber = 16;
    }
    
    else  if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == -5)   // 4th
    {
        melodicDeviceNumber = 17;
    }
    
    else  if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == -6)   // #4
    {
        melodicDeviceNumber = 18;
    }
    
    else  if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == -7)   //5th
    {
        melodicDeviceNumber = 19;
    }
    
    else  if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == -8)   // #5th
    {
        melodicDeviceNumber = 20;
    }
    
    else  if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == -9)   // 6th
    {
        melodicDeviceNumber = 21;
    }
    
    else  if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == -10)   // b7th
    {
        melodicDeviceNumber = 22;
    }
    
    else  if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == -11)   // 7th
    {
        melodicDeviceNumber = 23;
    }

    else
    {
        melodicDeviceNumber = -1;
    }
    
    
    if (melodicDeviceNumber != -1)
    {
        
//        for (int count = 0; count < 2; count ++)
//        {
//            std::cout << "Interval = " << sequenceNotes[noteCounter + count] << std::endl;
//        }
        
    }
    
    
    return melodicDeviceNumber;
}
