/*
  ==============================================================================

    RhythmLibrary.h
    Created: 11 Mar 2017 8:40:07am
    Author:  Thomas Harvey

  ==============================================================================
*/


#include "../JuceLibraryCode/JuceHeader.h"
#include <stdio.h>


#ifndef RHYTHMLIBRARY_H_INCLUDED
#define RHYTHMLIBRARY_H_INCLUDED


class RhythmLibrary
{
public:
    
    RhythmLibrary();
    ~RhythmLibrary();
    
    
    struct RhythmProperties
    {
        Array<int> noteOn;
        Array<int> noteOff;
        Array<int> rest;
        int length;
        int longRest;
    };
    
    RhythmProperties returnRhythm(int rhythmChoice);
    
    
private:
    
    RhythmProperties rhythms[20];
    RhythmProperties rhythms2[20];
    


};



#endif  // RHYTHMLIBRARY_H_INCLUDED
