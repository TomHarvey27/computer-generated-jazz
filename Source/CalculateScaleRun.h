/*
  ==============================================================================

    CalculateScaleRun.h
    Created: 22 Feb 2017 6:48:18pm
    Author:  Thomas Harvey

  ==============================================================================
*/
#include "../JuceLibraryCode/JuceHeader.h"

#ifndef CALCULATESCALERUN_H_INCLUDED
#define CALCULATESCALERUN_H_INCLUDED


class CalculateScaleRun
{
    
public:
    
    CalculateScaleRun();
    ~CalculateScaleRun();
    
    int calculateScaleRun(Array<int> sequenceNotes, int noteCounter, Array<int> endOfSequence);
    void calculateMelodicDeviceNumber(Array<int> sequenceNotes, int noteCounter);
    
private:
    
    int scaleRunLooper = 0;
    int direction = 0;
    int melodicDeviceNumber = 0;
    int diatonicNotes[7] = {0 ,2 ,4 ,5 ,7 ,9 , 11};
    bool noteIsChromatic = false;
    
};


#endif  // CALCULATESCALERUN_H_INCLUDED
