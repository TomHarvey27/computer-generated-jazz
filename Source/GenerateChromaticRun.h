/*
 ==============================================================================
 
 GenerateChromaticRun.h
 Created: 21 Dec 2016 12:52:21pm
 Author:  Thomas Harvey
 
 ==============================================================================
 */

#include "../JuceLibraryCode/JuceHeader.h"

#ifndef GENERATECHROMATICRUN_H_INCLUDED
#define GENERATECHROMATICRUN_H_INCLUDED
#include <vector>
using std::vector;

class GenerateChromaticRun
{
    
public:
    
    GenerateChromaticRun();
    ~GenerateChromaticRun();
    
    void calculateChromaticRun(int currentNote_, int direction_, bool tooHigh, bool tooLow , int randNum);
    void setChromaticProbabilities(int chromaticCounter[6],int changeDirectionThreeNotes[4], int changeDirectionFourNotes[6] , int changeDirectionFiveNotes[12]);
    
    int (* orderArrayPointer)[2] = nullptr;
    
    int lastNote = 0;
    Array <int> chromaticArray;

    
private:
    
    Random random;
    
    int orderOfNotesThree[4][3] =
    
    {{0,1,1},
        {0,1,-1},
        {0,-1,1},
        {0,-1,-1}};
    
    // potentially add in different orders
    int orderOfNotesFour[6][4] =
    
    {{0,1,1, 1}, // H
        {0,1,1,-1}, // H
        {0,1,-1,-1}, // L
        {0,-1,1,1}, // H
        {0,-1,-1,1}, // L
        {0,-1,-1,-1}}; // L
    
    int orderOfNotesFive[16][5] =
    
       {{0, 1, 1, 1, 1}, // H
        {0, 1, 1, 1,-1}, // H
        {0, 1, 1,-1, 1}, // H
        {0,-1, 1, 1, 1}, // H
        {0, 1,-1, 1, 1}, // H
        {0, 1, 1,-1,-1}, // S
        {0, 1,-1,-1, 1}, // S
        {0,-1,-1, 1, 1}, // S
        {0,-1, 1, 1,-1}, // S
        {0, 1,-1,-1,-1}, // L
        {0,-1, 1,-1,-1}, // L
        {0,-1,-1,-1, 1}, // L
        {0,-1,-1, 1,-1}, // L
        {0,-1,-1,-1,-1}}; // L
    
    vector <int> chromaticCounters;
    vector <int> threeNoteProbabilities;
    vector <int> fourNoteProbabilities;
    vector <int> fiveNoteProbabilities;
    
    int sumOfProbabilities[3] = {0};
    
    int sumOfChromaticCounters = 0;
    int SUM = 0;
};




#endif  // GENERATECHROMATICRUN_H_INCLUDED
