/*
  ==============================================================================

    CalculateChromaticRun.h
    Created: 22 Feb 2017 6:48:06pm
    Author:  Thomas Harvey

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"

#ifndef CALCULATECHROMATICRUN_H_INCLUDED
#define CALCULATECHROMATICRUN_H_INCLUDED

class CalculateChromaticRun
{
    
public:
    
    CalculateChromaticRun();
    ~CalculateChromaticRun();
    
    int calculateChromaticRun(Array<int> sequenceNotes, int noteCounter, Array<int> endOfSequence);
    void calculateMelodicDeviceNumber(Array<int> sequenceNotes, int noteCounter);
    
private:
    
    int chromaticRunLooper = 0;
    int direction = 0;
    int melodicDeviceNumber = 0;
    
};



#endif  // CALCULATECHROMATICRUN_H_INCLUDED
