/*
  ==============================================================================

    CalculateInterval.h
    Created: 22 Feb 2017 6:48:32pm
    Author:  Thomas Harvey

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"

#ifndef CALCULATEINTERVAL_H_INCLUDED
#define CALCULATEINTERVAL_H_INCLUDED

class CalculateInterval
{
    
public:
    
    CalculateInterval();
    ~CalculateInterval();
    
    int calculateInterval(Array<int> sequenceNotes, int noteCounter);
    int calculateMelodicDeviceNumber(Array<int> sequenceNotes, int noteCounter);
    
private:
    
    int diatonicNotes[7] = {0 ,2 ,4 ,5 ,7 ,9 , 11};
    int melodicDeviceNumber = -1;
    bool nextNoteScaleDegree = false;
    
};



#endif  // CALCULATEINTERVAL_H_INCLUDED
