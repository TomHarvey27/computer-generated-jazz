/*
  ==============================================================================

    CalculateOutOfKeyInterval.h
    Created: 22 Feb 2017 6:49:06pm
    Author:  Thomas Harvey

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#ifndef CALCULATEOUTOFKEYINTERVAL_H_INCLUDED
#define CALCULATEOUTOFKEYINTERVAL_H_INCLUDED


class CalculateOutOfKeyInterval
{

public:
    
    CalculateOutOfKeyInterval();
    ~CalculateOutOfKeyInterval();
    
    int calculateMelodicDeviceNumber(Array<int> sequenceNotes, int noteCounter);
    
private:
    
    int diatonicNotes[7] = {0 ,2 ,4 ,5 ,7 ,9 , 11};
    int melodicDeviceNumber = -1;
    bool nextNoteScaleDegree = false;
    
};


#endif  // CALCULATEOUTOFKEYINTERVAL_H_INCLUDED
