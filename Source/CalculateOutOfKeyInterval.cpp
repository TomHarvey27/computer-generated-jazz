/*
  ==============================================================================

    CalculateOutOfKeyInterval.cpp
    Created: 22 Feb 2017 6:49:06pm
    Author:  Thomas Harvey

  ==============================================================================
*/

#include "CalculateOutOfKeyInterval.h"

CalculateOutOfKeyInterval::CalculateOutOfKeyInterval()
{
    
}

CalculateOutOfKeyInterval::~CalculateOutOfKeyInterval()
{
    
}


int CalculateOutOfKeyInterval::calculateMelodicDeviceNumber(Array<int> sequenceNotes, int noteCounter)
{

    
    
    
    
    if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == 2 )   // if it goes down a diatonic Second
    {
        melodicDeviceNumber = 15;
        std::cout << "Down a second" << std::endl;
    }
 
    
    else if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == -2 )   // if it goes up a diatonic Second
    {
        melodicDeviceNumber = 16;
        std::cout << "Up a second" << std::endl;
    }
    
    else if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == 3 ||
        sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == 4)   // if it goes down a diatonic third
    {
        melodicDeviceNumber = 17;
        std::cout << "Down a third" << std::endl;
    }
    
    else  if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == -3 ||
              sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == -4)   // if it goes up a diatonic third
    {
        melodicDeviceNumber = 18;
        std::cout << "Up a third" << std::endl;
    }
    
    else if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == 5 ||
             sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == 6)   // if it goes down a diatonic 4th
    {
        std::cout << "down a fourth" << std::endl;
        melodicDeviceNumber = 19;
    }
    
    
    else if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == -5 ||
             sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == -6)   // if it goes up a diatonic 4th
    {
        std::cout << "up a fourth" << std::endl;
        melodicDeviceNumber = 20;
    }
    
    else if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == 7 ||
             sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == 8)   // if it goes down a diatonic 5th
    {
        melodicDeviceNumber = 21;
        std::cout << "down a fifth" << std::endl;
    }
    
    else if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == -7 ||
             sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == -8)   // if it goes up a diatonic 5th
    {
        melodicDeviceNumber = 22;
        std::cout << "up a fifth" << std::endl;
    }
    
    else if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == 9)  // if it goes down a diatonic 6th
    {
        melodicDeviceNumber = 23;
        std::cout << "down a sixth" << std::endl;
    }
    
    else if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == - 9)  // if it goes up a diatonic 6th
    {
        melodicDeviceNumber = 24;
    }
    
    else if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == 10 ||
             sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == 11)   // if it goes down a diatonic 7th
    {
        melodicDeviceNumber = 25;
    }
    
    else if (sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == -10 ||
             sequenceNotes[noteCounter] - sequenceNotes[noteCounter + 1] == -11)   // if it goes up a diatonic 7th
    {
        melodicDeviceNumber = 26;
    }
    
    if (melodicDeviceNumber != -1)
    {
        
        for (int count = 0; count < 2; count ++)
        {
            std::cout << "Out of key Interval = " << sequenceNotes[noteCounter + count] << std::endl;
        }
        
    }
    
    return melodicDeviceNumber;
    
}
