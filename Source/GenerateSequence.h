/*
  ==============================================================================

    GenerateSequence.h
    Created: 23 Feb 2017 8:31:19am
    Author:  Thomas Harvey

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include <stdio.h>
#include "RhythmLibrary.h"
#include <vector>
using std::vector;

#ifndef GENERATESEQUENCE_H_INCLUDED
#define GENERATESEQUENCE_H_INCLUDED

class GenerateSequence
{
    
public:
    
    GenerateSequence();
    ~GenerateSequence();
    
    void setNotesInSequence(int chordNumber_, int chordDuration_, int key_, int nextChord_, int nextKey_);
    void setDirection();
    void setCurrentNote();
    void findNearestChordTone(int chordNumber_, int key_);
    void addInterval(int intervalNumber);
    void setChordProbabilities(int melodicDevicePerChord[12][24], int chord);
    void generateRhythm(int totalRhythm, Array<int> chordDuration_, int songRepeat);
    
    struct NoteProperties
    {
        
        Array<int> noteNumber;
        Array<int> noteOn;
        Array<int> noteOff;
        Array<int> rest;
        Array<int> firstNoteOfChord; // 1 means it is, 0 means everything else
        Array<int> barPosition;
        
    };
    
    Random random;
    
    Array<int> finalSequence;
    int firstTimeCount = 0;
    
     GenerateSequence::NoteProperties sequenceRhythm; // the skeloton rhythm for the entire sequence

private:
    
    
    bool firstTime = true;
    bool firstNote = false;
    int chordDuration;
    int chord = 0;
    int nextChord = 0;
    int nextKey = 0;
    
    Array<int> chordTimeStamps;
    int chordCounter = 0;
    
    int diatonicNotes[7] = {0 ,2 ,4 ,5 ,7 ,9 , 11};
    
    int direction = 0;
    int randNum = 0;
    int sequenceDuration = 0;
    
    int sequenceCounter = 0;
    int key = 0;
    
    int testCoutner = 0;

    int sumOfMelodicDeiveMinor[12] = {0};
    int sumOfMelodicDeiveDominant[12] = {0};
    int sumOfMelodicDeiveMajor[12] = {0};
    
    vector<vector<int> > melodicDeviceMinorProbabilities;
    vector<vector<int> > melodicDeviceDominantProbabilities;
    vector<vector<int> > melodicDeviceMajorProbabilities;
    
    int minorVectorCounter = 0;
    int previousNote = 72;
  
    int chordTones[5][5] ={
        
        {0,4,7,11,12}, // Major1
        {2,5,9,0,12},   //Minor2
        {7,11,2,5,14},  //Dominant5
        {7,11,2,5,14} ,
        {7,11,2,5,14}};
    
    int seed = 0;
    
    int rhythmCounter = 0;
    int previousNoteOff = 0;
    
    Array<int> rhythm1Values;
    
    int rhythm1Probabilities[20] = {
        123,
        78,
        13,
        13,
        8,
        8,
        8,
        8,
        8,
        7,
        7,
        7,
        7,
        7,
        7,
        7,
        6,
        6,
        6,
        6};


    RhythmLibrary rhythmLibrary;    // Used to call the function to select what rhythm
    RhythmLibrary::RhythmProperties rhythms;    // Used to store the retrived rythm
    

    
    int longRhythmCounter = 0;
    int chromaticCounter = 0;
    
};




#endif  // GENERATESEQUENCE_H_INCLUDED
