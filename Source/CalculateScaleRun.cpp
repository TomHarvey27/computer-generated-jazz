/*
  ==============================================================================

    CalculateScaleRun.cpp
    Created: 22 Feb 2017 6:48:18pm
    Author:  Thomas Harvey

  ==============================================================================
*/

#include "CalculateScaleRun.h"

CalculateScaleRun::CalculateScaleRun()
{
    
}

CalculateScaleRun::~CalculateScaleRun()
{
    
}

int CalculateScaleRun::calculateScaleRun(Array<int> sequenceNotes, int noteCounter, Array<int> endOfSequence)
{
    scaleRunLooper = 0;
    int scaleRunCounter = 0;
    int accending = 1;
    int decending = -1;
    int direction = 0;
    bool isNoteDecending = true;
    bool isNoteAccending = true;
    
    for (int count = 0; count < 7; count ++)    // checks if the note is a diatonic (we already know that)
    {
        if ((sequenceNotes[noteCounter + scaleRunLooper] - diatonicNotes[count]) % 12 == 0)
        {
            scaleRunCounter = count;
        }
    }

    while(true) // note at end of sequence;
    {
        
        scaleRunLooper ++;
        // std::cout << "Note to check = " << sequenceNotes[noteCounter + scaleRunLooper] << std::endl;
        
        if (endOfSequence[(noteCounter + scaleRunLooper -1)] == 0) // sort out this loop properly in another test
        {
            scaleRunLooper --;
            noteIsChromatic = false;
            calculateMelodicDeviceNumber(sequenceNotes, noteCounter);
            return melodicDeviceNumber;
        }
        
        if (scaleRunCounter == 0) // if it is the first note and decending, wrap around to the first
        {
            accending = 1;
            decending = 6;
        }
        
        else if (scaleRunCounter == 6)
        {
            accending = -6;
            decending = -1;
        }
        
        else
        {
            accending = 1;
            decending = -1;
        }
        
        
        if ((sequenceNotes[noteCounter + scaleRunLooper] - diatonicNotes[scaleRunCounter + accending]) % 12 == 0 && isNoteAccending == true) // if the note is accending
        {
            //  DBG("Accending");
            scaleRunCounter ++;
            
            if (scaleRunCounter > 6)
            {
                scaleRunCounter = 0;
            }
            
            isNoteDecending = false; // makes sure it exits if the note changes direction but remains a scalerun
            direction = 1;
        }
        
        // if note is 0 and decending add 6 so it wraps around
        else if ((sequenceNotes[noteCounter + scaleRunLooper] - diatonicNotes[scaleRunCounter + decending]) % 12 == 0 && isNoteDecending == true) // if the note is decending
        {
            // DBG("Decending");
            scaleRunCounter --;
            
            if (scaleRunCounter < 0)
            {
                scaleRunCounter = 6;
            }
            isNoteAccending = false; // makes sure it exits if the note changes direction but remains a scalerun
            direction = -1;
        }
        
        else    // if its not part of a scale run
        {
            
            if (scaleRunLooper == 1)
            {
                noteIsChromatic = false;
                return -1;   // exit if its not part of a scale run at all
            }
            
            if (sequenceNotes[noteCounter + scaleRunLooper] - sequenceNotes[noteCounter + scaleRunLooper - 1] == direction ) // if the note is a semi tone away
            {
                
                
                if (scaleRunLooper == 2) // it can only be chromatic if theres been two notes
                {
                    if (sequenceNotes[noteCounter + scaleRunLooper - 1] -
                        sequenceNotes[noteCounter + scaleRunLooper - 2] == - 1) // if the first two are a semi tone apart
                    {
                        direction = -1;
                    }
                    
                    else if (sequenceNotes[noteCounter + scaleRunLooper - 1] -
                             sequenceNotes[noteCounter + scaleRunLooper - 2] ==  1 )
                    {
                        direction = 1;
                    }
                    
                    else    // if the previous two were not chromatic
                    {
                        noteIsChromatic = false;
                        calculateMelodicDeviceNumber(sequenceNotes, noteCounter);
                        return melodicDeviceNumber;
                    }
                    
                    scaleRunLooper ++;
                    
                    for (int count = scaleRunLooper; count < 5; count ++)
                    {
                        if (sequenceNotes[noteCounter + count] - sequenceNotes[noteCounter + count - 1] == direction)
                        {
                            std::cout << count << " note chromatic sequence " << std::endl;
                            scaleRunLooper ++;
                        }
                        
                        else
                        {
                            noteIsChromatic = true;
                            calculateMelodicDeviceNumber(sequenceNotes, noteCounter);
                            return melodicDeviceNumber; // calculate chromatic looper
                        }
                        
                    }
                    
                    
                }
                
                else    // if it is more than two notes it can't be a chromatic
                {
                    noteIsChromatic = false;
                    calculateMelodicDeviceNumber(sequenceNotes, noteCounter);
                    return melodicDeviceNumber;
                }
                
                
            }
            
            else // if the next note isn't chromatically related
            {
                noteIsChromatic = false;
                calculateMelodicDeviceNumber(sequenceNotes, noteCounter);
                return melodicDeviceNumber;
            }

            noteIsChromatic = true;
            calculateMelodicDeviceNumber(sequenceNotes, noteCounter);
            return melodicDeviceNumber;   // it will reach here if a 5 note chromatic number has been found
        }
        
        
    }
    
}

void CalculateScaleRun::calculateMelodicDeviceNumber(Array<int> sequenceNotes, int noteCounter)
{
    
    if (noteIsChromatic == false && scaleRunLooper > 1)
    {

    for (int count = 0; count < scaleRunLooper; count ++)
    {
        std::cout << "Scale run = " << sequenceNotes[count + noteCounter] << std::endl;
    }
    
    if (scaleRunLooper == 2 && sequenceNotes[noteCounter] < sequenceNotes[noteCounter + 1])
    {
        melodicDeviceNumber = 13;
    }
    
    else if (scaleRunLooper == 2 && sequenceNotes[noteCounter] > sequenceNotes[noteCounter + 1])
    {
        melodicDeviceNumber = 14;
    }
    
    else if (scaleRunLooper == 3 && sequenceNotes[noteCounter] < sequenceNotes[noteCounter + 1])
    {
        melodicDeviceNumber = 15;
    }
    
    else if (scaleRunLooper == 3 && sequenceNotes[noteCounter] > sequenceNotes[noteCounter + 1])
    {
        melodicDeviceNumber = 16;
    }
    
    else if (scaleRunLooper == 4 && sequenceNotes[noteCounter] < sequenceNotes[noteCounter + 1])
    {
        melodicDeviceNumber = 17;
    }
    
    else if (scaleRunLooper == 4 && sequenceNotes[noteCounter] > sequenceNotes[noteCounter + 1])
    {
        melodicDeviceNumber = 18;
    }
    
    else if (scaleRunLooper == 4 && sequenceNotes[noteCounter] < sequenceNotes[noteCounter + 1])
    {
        melodicDeviceNumber = 19;
    }
    
    else if (scaleRunLooper == 4 && sequenceNotes[noteCounter] > sequenceNotes[noteCounter + 1])
    {
        melodicDeviceNumber = 20;
    }
    
    else if (scaleRunLooper == 5  && sequenceNotes[noteCounter] < sequenceNotes[noteCounter + 1])
    {
        melodicDeviceNumber = 21;
    }
    
    else if (scaleRunLooper == 5 && sequenceNotes[noteCounter] > sequenceNotes[noteCounter + 1])
    {
        melodicDeviceNumber = 22;
    }
    
    else if (scaleRunLooper > 5 && sequenceNotes[noteCounter] < sequenceNotes[noteCounter + 1])   // if its any bigger than four increment the 5 counter
    {
        melodicDeviceNumber = 23;
    }
    
    else if (scaleRunLooper > 5 && sequenceNotes[noteCounter] > sequenceNotes[noteCounter + 1])
    {
        melodicDeviceNumber = 24;
    }
    scaleRunLooper = 0;
    
    }
    
    
    else    // if the note is chromatic 
    {
        
        if (scaleRunLooper == 3 && sequenceNotes[noteCounter] < sequenceNotes[noteCounter + 1])
        {
            melodicDeviceNumber = 27;
        }
        
        else if (scaleRunLooper == 3 && sequenceNotes[noteCounter] > sequenceNotes[noteCounter + 1])
        {
            melodicDeviceNumber = 28;
        }
        
        else if (scaleRunLooper == 4 && sequenceNotes[noteCounter] < sequenceNotes[noteCounter + 1])
        {
            melodicDeviceNumber = 29;
        }
        
        else if (scaleRunLooper == 4 && sequenceNotes[noteCounter] > sequenceNotes[noteCounter + 1])
        {
            melodicDeviceNumber = 30;
        }
        
        else if (scaleRunLooper == 4 && sequenceNotes[noteCounter] < sequenceNotes[noteCounter + 1])
        {
            melodicDeviceNumber = 31;
        }
        
        else if (scaleRunLooper == 4 && sequenceNotes[noteCounter] > sequenceNotes[noteCounter + 1])
        {
            melodicDeviceNumber = 32;
        }
        
        else if (scaleRunLooper == 5  && sequenceNotes[noteCounter] < sequenceNotes[noteCounter + 1])
        {
            melodicDeviceNumber = 33;
        }
        
        else if (scaleRunLooper == 5 && sequenceNotes[noteCounter] > sequenceNotes[noteCounter + 1])
        {
            melodicDeviceNumber = 34;
        }
        
        else if (scaleRunLooper > 5 && sequenceNotes[noteCounter] < sequenceNotes[noteCounter + 1])   // if its any bigger than four increment the 5 counter
        {
            melodicDeviceNumber = 35;
        }
        
        else if (scaleRunLooper > 5 && sequenceNotes[noteCounter] > sequenceNotes[noteCounter + 1])
        {
            melodicDeviceNumber = 36;
        }
  
        
    }
}
