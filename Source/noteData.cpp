/*
 ==============================================================================
 
 noteData.cpp
 Created: 10 Nov 2016 7:58:59am
 Author:  Thomas Harvey
 
 ==============================================================================
 */

#include "NoteData.h"

noteData::noteData()
{
    
    for (int count = 0 ; count < 12; count ++)
    {
        for (int counter = 0 ; counter < 12; counter ++)
        {
            countersForEachNote[count][counter] = 0;
        }
    }
    
    for (int count = 0 ; count < 12; count ++)
    {
        for (int counter = 0 ; counter < 12; counter ++)
        {
            countersForMelodicDevice[count][counter] = 0;
        }
    }
    
}
noteData::~noteData()
{
    
    
}

void noteData::calculateBiggest(int note)
{
    if (note > biggest) // works out if it is the biggest note
    {
        biggest = note;     //makes the new biggest note
    }
}

void noteData::calculateSmallest(int note)
{
    
    if (note < smallest)
    {
        smallest = note;
    }
    
}
void noteData::addNumber(int note)
{
    sequenceNotes.add(note);
}

void noteData::isNoteDiatonic()
{
    
    int melodicDeviceNumber = 0;
    
    std::cout << "note to check " << sequenceNotes[noteCounter] << std::endl;
    
    if(diatonicChecker == false)
    {
        for (int count = 0; count < 7; count ++)       // Checks to see if it is a diatonic note
        {
            if ((sequenceNotes[noteCounter] - diatonicNotes[count]) % 12 == 0 )
                
            {
                diatonicChecker = true;
            }
        }
    }
    
    for (int count = 0; count < 12; count ++)  // Checks to see what note it is
    {
        if ((sequenceNotes[noteCounter] - count) % 12 == 0 )
        {
            currentNoteValue = count;
        }
    }
    
    if (diatonicChecker == true)   // if the note is diatonic
    {
        
        melodicDeviceNumber = interval.calculateInterval(sequenceNotes, noteCounter);   // then checks each type of interval
        
        if (melodicDeviceNumber == -1)
        {
            melodicDeviceNumber = scaleRun.calculateScaleRun(sequenceNotes, noteCounter, endOfSequence);
            
            if (melodicDeviceNumber == -1)
            {
                 melodicDeviceNumber = chromaticRun.calculateChromaticRun(sequenceNotes, noteCounter, endOfSequence);
                
                
                if (melodicDeviceNumber == -1)
                {
                    melodicDeviceNumber = outOfKeyInterval.calculateMelodicDeviceNumber(sequenceNotes, noteCounter);
                }
            }
        
        }

        if (melodicDeviceNumber != -1)
        {
              melodicDevicePerChord[currentNoteValue][melodicDeviceNumber] ++;
        }
      
        else
        {
            // counter for how many that didn't go through?
        }
        
    }
    
    else // if it isn't diatonic
    {
        
        melodicDeviceNumber = chromaticRun.calculateChromaticRun(sequenceNotes, noteCounter, endOfSequence);
        
        
        if (melodicDeviceNumber == -1)
        {
            melodicDeviceNumber = outOfKeyInterval.calculateMelodicDeviceNumber(sequenceNotes, noteCounter);
        }
        
            melodicDevicePerChord[currentNoteValue][melodicDeviceNumber] ++;
    }
    diatonicChecker = false;
}

void noteData::calculateMelodicDevice()
{
 
    for (noteCounter = 0; noteCounter < sequenceNotes.size(); noteCounter ++)    // This increments the note counters for each note
        
    {
        if(endOfSequence[noteCounter] != 0) // If its the last note in the sequence it doesn't check it
            
        {
           isNoteDiatonic();   // calculates what type of melodic device the note is.
        }
    }
    
}


void noteData::melodicDeviceCounters(int melodicDevice, int order)
{

    
}


void noteData::printNotes(int order)
{
    
    
}

void noteData::noteCounters(int note, int order)
{
    
    //    int secondNote = sequenceNotes[noteCounter + 1];
    //    int scaleDegree1, scaleDegree2 = 0;
    //
    //    for (int count = 0; count < 12; count ++)
    //    {
    //
    //        if(order == 0)
    //        {
    //            note = sequenceNotes[noteCounter];
    //
    //            if ((note - count ) % 12 == 0) // If it matches it calls the function again
    //            {
    //                noteCounters(count, 1); // work out what note it is 0 -11
    //            }
    //
    //        }
    //
    //        if (order == 1)
    //        {
    //
    //
    //            if ((secondNote - count ) % 12 == 0) // If it matches, increment its counter at the next notes position
    //            {
    //                countersForEachNote[note][count] ++;
    //                secondNote = count;
    //            }
    //
    //
    //        }
    //    }
    //
    //
    //    scaleDegree1 = returnScaleDegree(note);
    //    scaleDegree2 = returnScaleDegree(secondNote);
    //
    //    note = sequenceNotes[noteCounter];              // turns them into actual notes again
    //    secondNote =  sequenceNotes[noteCounter + 1];
    //
    //    if (order == 1 || scaleDegree1 != -1 || scaleDegree2 != -1) // if both notes are diatonic
    //    {
    //        if (note - secondNote == 3 || note - secondNote == 4 || note - secondNote == -9 || note - secondNote == -8)   // If a third or sixth away
    //        {
    //            thirdDyadSelected[scaleDegree2] ++;
    //        }
    //
    //        else if (note - secondNote == -3 || note - secondNote == -4 || note - secondNote == 9 || note - secondNote == 8)
    //        {
    //            thirdDyadSelected[scaleDegree1] ++;
    //        }
    //
    //        else if (note - secondNote == 5 || note - secondNote == -7)
    //        {
    //            fifthDyadSelected[scaleDegree1] ++;
    //        }
    //
    //        else if (note - secondNote == -5 || note - secondNote == 7)
    //        {
    //            fifthDyadSelected[scaleDegree2] ++;
    //        }
    //        
    //    }
    //    
    //    
    
    
}

int noteData::isNoteAthird()
{
    
}


int noteData::calculateInterValRelationship()
{
   
}




int noteData::calculateScaleRun()
{
    
}

int noteData::calculateChromaticRun()
{
    
}

int noteData::calculateOutOfKeyInterval()
{

}



int noteData::selectTriad(int triadNumber, int count)
{

    
}

int noteData::returnScaleDegree(int triadCounter_)
{

    
}

bool noteData::returnTriad(int counter)
{

}

void noteData::setTriadsTrue(int count)
{

}

void noteData::setTriadsFalse(int count)
{

}


void noteData::resetCounters()
{
    
    int count = 0;
    cCount[count] = 0;
    csCount[count] = 0;
    dCount[count] = 0;
    dsCount[count] = 0;
    eCount[count] = 0;
    fCount[count] = 0;
    fsCount[count] = 0;
    gCount[count] = 0;
    gsCount[count] = 0;
    aCount[count] = 0;
    asCount[count] = 0;
    bCount[count] = 0;
}

int noteData::melodicDeviceOrder(int type, int length) // use type
{
    
}
