/*
 ==============================================================================
 
 noteData.h
 Created: 10 Nov 2016 7:58:59am
 Author:  Thomas Harvey
 
 ==============================================================================
 */

#ifndef NOTEDATA_H_INCLUDED
#define NOTEDATA_H_INCLUDED

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "CalculateOutOfKeyInterval.h"
#include "CalculateChromaticRun.h"
#include "CalculateInterval.h"
#include "CalculateScaleRun.h"

class noteData


{
    
    
public:
    
    noteData();
    ~noteData();
    void calculateBiggest(int note);
    void calculateSmallest(int note);
    void addNumber(int note);
    void noteCounters(int note, int order);
    void printNotes(int order);
    void resetCounters();
    void melodicDeviceCounters(int melodicDevice, int order);
    void calculateMelodicDevice();
    
    
    
    int isNoteAthird();
    
    
    void isNoteDiatonic();
    int calculateInterValRelationship();
    
    int selectTriad(int triadNumber, int count);
    int isNotePartOfaTriad(int LoopNumber_);
    int returnScaleDegree(int triadCounter_);
    
    bool returnTriad(int counter);
    
    void setTriadsFalse(int count);
    void setTriadsTrue(int count);
    int selecetPossibleTriads(int triadNumber, int count);
    
    
    int calculateScaleRun();
    int calculateChromaticRun();
    int calculateOutOfKeyInterval();
    
    int melodicDeviceOrder(int type, int length);
    
    
    
    Array <int> sequenceNotes;
    String noteName[12] = {"C"};
    
    int noteCounter;
    
    int countersForEachNote[12][12]; // 0 = c, 1 C# ect
    
    int countersForMelodicDevice[6][6]; // Triad, Third, Fifth, Scale Run, Chromatic, Out of key
    
    int triadCounter[4] = {0};  // 3,4,5, other
    int fifthCounter[4] = {0};  // 2,3,4, other
    int thirdCounter[4] = {0}; //2,3,4, other
    int scaleRunCounter[6] = {0}; // 2,3,4,5,6,7
    int chromaticCounter[6] = {0}; // 2,3,4,5,6,7
    
    
    float triadChangeDirection[3]; // each element is a length of triad and will have a value 0 or 1 thenwill be dived by its triadcounter
    float fifthChangeDirection[3];
    float thirdChangeDirection[3];
    
    
    int scaleRunChangeDirectionThreeNotes[4] = {0};
    int chromaticChangeDirectionThreeNotes[4] = {0};
    int scaleRunChangeDirectionFourNotes[6] = {0};
    int chromaticChangeDirectionFourNotes[6] = {0};
    int scaleRunChangeDirectionFiveNotes[12] = {0};
    int chromaticChangeDirectionFiveNotes[12] = {0};
    
    
    
    
    
    int cCount[11] = {0};
    int csCount[11] = {0};
    int dCount[11] = {0};
    int dsCount[11] = {0};
    int eCount[11] = {0};
    int fCount[11] = {0};
    int fsCount[11] = {0};
    int gCount[11] = {0};
    int gsCount[11] = {0};
    int aCount[11] = {0};
    int asCount[11] = {0};
    int bCount[11] = {0};
    
    int diatonicNotes[7] = {0 ,2 ,4 ,5 ,7 ,9 , 11};
    int outOfKeyNotes[5] = {1,3,6,8,10};
    
    int firstTriad[3] = {0,4,7};
    int secondTriad[3] = {2,5,9};
    int thirdTriad[3] = {4,7,11};
    int fourthTriad[3] = {5,9,0};
    int fithTriad[3] = {7,11,2};
    int sixthTriad[3] = {9,0,4};
    int seventhTriad[3] = {11,2,5};
    
    
    int firstTriadPossible[3] = {0,5,9};
    int secondTriadPossible[3] = {2,7,11};
    int thirdTriadPossible[3] = {4,9,0};
    int fourthTriadPossible[3] = {5,11,2};
    int fithTriadPossible[3] = {7,0,4};
    int sixthTriadPossible[3] = {9,2,5};
    int seventhTriadPossible[3] = {11,4,7};
    
    
    
    bool diatonicChecker = false;
    bool FirstTriad = true;
    bool SecondTriad = true;
    bool ThirdTriad = true;
    bool FourthTriad = true;
    bool FithTriad = true;
    bool SixthTriad = true;
    bool SeventhTriad = true;
    
    
    bool thirdDyad;
    
    bool FirstNote;
    bool SecondNote;
    bool ThirdNote;
    
    bool noteIsChromatic;
    
    bool exitLoop = false;
    
    int firstNote = 0;
    int secondNote = 0;
    int thirdNote = 0;
    
    Array<int> triad;
    Array <int> currentTriad;
    int triadLooper = 0;
    
    
    Array<int> endOfSequence;   // make into juce arrays
    int startOfSequence[200] = {1};
    
    int firstElementOfTriad = 0;
    
    
    int scaleRunCurrentNote = 10;
    int scaleRunLooper = 0;
    
    Array<int> scaleRunSequence;
    
    int chromaticRunLooper = 0;
    
    int previousMelodicDevice = 0;
    int currentMelodicDevice = 0;
    int noteCounterIncrementValue = 0;
    
    
    int melodicDevicePerChord[12][80] =

    {{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0}};
    int currentNoteValue = 0;
    
    
private:
    
    int thirdDyadSelected[7] = {0}; // C to B diatonicly
    int fifthDyadSelected[7] = {0};
    int triadSelected[7] = {0};
    
    int temporaryOrderOfNotesThree[3]  = {0};
    int temporaryOrderOfNotesFour[4] = {0};
    int temporaryOrderOfNotesFive[5] = {0};
    
    int temporaryChangeDirectionThreeNotes[4] = {0};
    int temporaryChangeDirectionFourNotes[6] = {0};
    int temporaryChangeDirectionFiveNotes[12] = {0};
    
    
    int orderOfNotesThree[4][3] =
    
    {{0,1,2},       // 42 , 31
        {0,1,0},    // 55, 57
        {0,-1,0},  // 54, 56
        {0,-1,-2}}; // 5, 16 , 31, 42
    
    // potentially add in different orders
    int orderOfNotesFour[6][4] =
    
    {{0,1,2, 3},    // 76, 78
        {0,1,2,1},   // 43 , 32
        {0,1,0,-1},     // 7, 18
        {0,-1,0,1},     // 44, 33
        {0,-1,-2,-1},   // 6 , 17
        {0,-1,-2,-3}}; // 8, 19
    
    int orderOfNotesFive[12][5] =
    
    {{0,1,2, 3,4},  // 36 , 42
        {0,1,2,3,2},
        {0,1,2,1,2},
        {0,1,2,1,0},
        {0,1,0,-1,0},
        {0,1,0,-1,-2},
        {0,-1,0,1,2},
        {0,-1,0,1,0},
        {0,-1,-2,-1,0},
        {0,-1,-2,-3,-2},
        {0,-1,-2,-1,-2},
        {0,-1,-2,-3,-4}};
  

    CalculateScaleRun scaleRun;
    CalculateChromaticRun chromaticRun;
    CalculateInterval interval;
    CalculateOutOfKeyInterval outOfKeyInterval;
    
    
    int biggest = 0;
    int smallest = 90;
    int noteCount;
    
    int loopNumber = 0;
    
};



#endif  // NOTEDATA_H_INCLUDED
