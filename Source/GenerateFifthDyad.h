/*
 ==============================================================================
 
 GenerateFifthDyad.h
 Created: 21 Dec 2016 12:51:55pm
 Author:  Thomas Harvey
 
 ==============================================================================
 */

#ifndef GENERATEFIFTHDYAD_H_INCLUDED
#define GENERATEFIFTHDYAD_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

class GenerateDyad
{
    
    
public:
    
    GenerateDyad();
    ~GenerateDyad();
    
    void calculateDyad(int currentNote_ , int chord_, int dyadType_, bool tooHigh, bool tooLow, int randNum);
    void setDyad(int dyad_[], int currentNote_);
    void setDyadProbabilities(int fithDyadCounter, int thirdDyadCounter); // work out different probabilities for dyads
    
    int lastNote = 0;
    Array <int> dyadArray;
    
    int (* dyadArrayPointer)[2] = nullptr;
    
    
private:
    
    Random randomDyad;
    
    int thirdDyad[7][2] =
    
    {{0,4},
        {2,5},
        {4,7},
        {5,9},
        {7,11},
        {9,0},
        {11,2}};
    
    int fifthDyad[7][2] =
    
    {{0,7},
        {2,9},
        {4,11},
        {5,0},
        {7,2},
        {9,4},
        {11,5}};
    
};


#endif  // GENERATEFIFTHDYAD_H_INCLUDED
