/*
  ==============================================================================

    ChordLibrary.h
    Created: 24 Feb 2017 6:55:52pm
    Author:  Thomas Harvey

  ==============================================================================
*/
#include "../JuceLibraryCode/JuceHeader.h"
#include <stdio.h>

#ifndef CHORDLIBRARY_H_INCLUDED
#define CHORDLIBRARY_H_INCLUDED

class ChordLibrary
{
public:
    
    ChordLibrary();
    ~ChordLibrary();
    
    struct SongProperties
    {
        Array<int> chordDuration; // how long each chord lasts in ticks
        Array<int> chordNumber; // what number it is within the scale
        Array<int> key;         // if the chord needs to be transposed
        int songLength;         // song length in miliseconds
    };
    
    SongProperties returnChords(int songChoice);
  
private:
    
    SongProperties giantSteps;
    SongProperties ladyBird;
    SongProperties twoFiveOneSix;

};



#endif  // CHORDLIBRARY_H_INCLUDED
