/*
 ==============================================================================
 
 GenerateThirdDyad.h
 Created: 21 Dec 2016 12:51:38pm
 Author:  Thomas Harvey
 
 ==============================================================================
 */

#include "../JuceLibraryCode/JuceHeader.h"

#ifndef GENERATETHIRDDYAD_H_INCLUDED
#define GENERATETHIRDDYAD_H_INCLUDED

class GenerateThirdDyad
{
    
    
public:
    
    GenerateThirdDyad();
    ~GenerateThirdDyad();
    
    int* calculateThirdDyad(int currentNote_ , int chord_);
    void setThirdDyad(int dyad_[], int currentNote_);
    
    int lastNote = 0;
    Array <int> thirdDyadArray;
    
private:
    
    Random randomDyad;
    
    int thirdDyad[7][2] =
    
    {{0,4},
        {2,5},
        {4,7},
        {5,9},
        {7,11},
        {9,0},
        {11,2}};
    
};



#endif  // GENERATETHIRDDYAD_H_INCLUDED
