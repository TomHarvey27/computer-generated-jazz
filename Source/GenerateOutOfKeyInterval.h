/*
  ==============================================================================

    GenerateOutOfKeyInterval.h
    Created: 9 Jan 2017 9:38:45am
    Author:  Thomas Harvey

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#ifndef GENERATEOUTOFKEYINTERVAL_H_INCLUDED
#define GENERATEOUTOFKEYINTERVAL_H_INCLUDED


class OutOfKeyInterval

{

    
public:
   
    OutOfKeyInterval();
    ~OutOfKeyInterval();
    void generateOutOfKeyInterval();

    
    int diatonicNotes[7] = {0 ,2 ,4 ,5 ,7 ,9 , 11};
    
    Array <int> outOfKeyArray;
   
    
    
private:

    
    
};


#endif  // GENERATEOUTOFKEYINTERVAL_H_INCLUDED
