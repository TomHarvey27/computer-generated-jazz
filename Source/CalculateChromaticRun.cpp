/*
  ==============================================================================

    CalculateChromaticRun.cpp
    Created: 22 Feb 2017 6:48:06pm
    Author:  Thomas Harvey

  ==============================================================================
*/

#include "CalculateChromaticRun.h"



CalculateChromaticRun::CalculateChromaticRun()
{
 
}

CalculateChromaticRun::~CalculateChromaticRun()
{
 
}

int CalculateChromaticRun::calculateChromaticRun(Array<int> sequenceNotes, int noteCounter, Array<int> endOfSequence)
{
    chromaticRunLooper = 0;
    direction = 0; // sets the direction anyway
    
    if(sequenceNotes[noteCounter + chromaticRunLooper] - sequenceNotes[noteCounter + chromaticRunLooper + 1] == 1 ||
       sequenceNotes[noteCounter + chromaticRunLooper] - sequenceNotes[noteCounter + chromaticRunLooper + 1] == - 1)
    {
        
        direction = sequenceNotes[noteCounter + chromaticRunLooper] - sequenceNotes[noteCounter + chromaticRunLooper + 1];
        
        while (true)
        {
            
            
            if (noteCounter != 0 && endOfSequence[(noteCounter + chromaticRunLooper -1)] == 0)
            {
                chromaticRunLooper --;
                calculateMelodicDeviceNumber(sequenceNotes, noteCounter);
                return melodicDeviceNumber;
            }
            
            if (sequenceNotes[noteCounter + chromaticRunLooper] - (sequenceNotes[noteCounter + chromaticRunLooper + 1]) == direction)
            {
                chromaticRunLooper ++;
            }
            
            else
            {
                chromaticRunLooper ++;
                calculateMelodicDeviceNumber(sequenceNotes, noteCounter);
                return melodicDeviceNumber;
            }
        }
    }
    
    else
    {
        return -1;
    }
}

void CalculateChromaticRun::calculateMelodicDeviceNumber(Array<int> sequenceNotes, int noteCounter)
{
   
    for (int count = 0; count < chromaticRunLooper; count ++)
    {
        std::cout << "Chromatic run = " << sequenceNotes[count + noteCounter] << std::endl;
    }
    
    if (chromaticRunLooper == 2 && sequenceNotes[noteCounter] < sequenceNotes[noteCounter + 1])
    {
        melodicDeviceNumber = 25;
    }
    
    else if (chromaticRunLooper == 2 && sequenceNotes[noteCounter] > sequenceNotes[noteCounter + 1])
    {
        melodicDeviceNumber = 26;
    }
    
    else if (chromaticRunLooper == 3 && sequenceNotes[noteCounter] < sequenceNotes[noteCounter + 1])
    {
        melodicDeviceNumber = 27;
    }
    
    else if (chromaticRunLooper == 3 && sequenceNotes[noteCounter] > sequenceNotes[noteCounter + 1])
    {
        melodicDeviceNumber = 28;
    }
    
    else if (chromaticRunLooper == 4 && sequenceNotes[noteCounter] < sequenceNotes[noteCounter + 1])
    {
        melodicDeviceNumber = 29;
    }
    
    else if (chromaticRunLooper == 4 && sequenceNotes[noteCounter] > sequenceNotes[noteCounter + 1])
    {
        melodicDeviceNumber = 30;
    }
    
    else if (chromaticRunLooper == 4 && sequenceNotes[noteCounter] < sequenceNotes[noteCounter + 1])
    {
        melodicDeviceNumber = 31;
    }
    
    else if (chromaticRunLooper == 4 && sequenceNotes[noteCounter] > sequenceNotes[noteCounter + 1])
    {
        melodicDeviceNumber = 32;
    }
    
    else if (chromaticRunLooper == 5  && sequenceNotes[noteCounter] < sequenceNotes[noteCounter + 1])
    {
        melodicDeviceNumber = 33;
    }
    
    else if (chromaticRunLooper == 5 && sequenceNotes[noteCounter] > sequenceNotes[noteCounter + 1])
    {
        melodicDeviceNumber = 34;
    }
    
    else if (chromaticRunLooper > 5 && sequenceNotes[noteCounter] < sequenceNotes[noteCounter + 1])   // if its any bigger than four increment the 5 counter
    {
        melodicDeviceNumber = 35;
    }
    
    else if (chromaticRunLooper > 5 && sequenceNotes[noteCounter] > sequenceNotes[noteCounter + 1])
    {
        melodicDeviceNumber = 36;
    }
    chromaticRunLooper = 0;

    
}
