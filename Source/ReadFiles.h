/*
  ==============================================================================

    ReadFiles.h
    Created: 23 Feb 2017 8:31:39am
    Author:  Thomas Harvey

  ==============================================================================
*/


#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "CalculateSequence.h"

#ifndef READFILES_H_INCLUDED
#define READFILES_H_INCLUDED

class ReadFiles
{
public:
    
    
    ReadFiles();
    ~ReadFiles();
    
    void readfile(int chordCounter);
    void resetCounters();
    
    CalculateSequence sequence;
    
private:
    
    int fileSize[4] = {19,13,19,0} ;// how many files for each chord // CHANGE 1 BACK TO 17
    int key[4] = {2,-5,0,0};
    int noteCount = 0;
    int startOfsequenceCounter = 0;
    
    MidiFile mFile[50];
    File maj7Melodies[19]; // Number of files to read from
    File min7Melodies[19];
    File dom7Melodies[13];
    File files[4];
    
  
    
};



#endif  // READFILES_H_INCLUDED
