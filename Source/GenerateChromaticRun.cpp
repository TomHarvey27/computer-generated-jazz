/*
 ==============================================================================
 
 GenerateChromaticRun.cpp
 Created: 21 Dec 2016 12:52:21pm
 Author:  Thomas Harvey
 
 ==============================================================================
 */

#include "GenerateChromaticRun.h"

GenerateChromaticRun::GenerateChromaticRun()
{
    
    
}

GenerateChromaticRun::~GenerateChromaticRun()
{
    
}


void GenerateChromaticRun::setChromaticProbabilities(int chromaticCounter[6], int changeDirectionThreeNotes[4], int changeDirectionFourNotes[6] , int changeDirectionFiveNotes[12])
{
    
    int chromaticVectorCounter = 0;
    
    for (int count = 0; count < 6; count ++)
    {
        sumOfChromaticCounters += chromaticCounter[count];
    }
    
    
    chromaticCounters.resize(sumOfChromaticCounters);
    
    for (int count = 0; count < 6; count ++)
    {
     
        for (int counter = 0; counter < chromaticCounter[count] ; counter ++)
        {
            chromaticCounters[chromaticVectorCounter] = count + 2;
            chromaticVectorCounter ++;
        }
        
    }
    
    for (int count = 0; count < 4; count ++)
    {
        sumOfProbabilities[0] += changeDirectionThreeNotes[count];
    }
    
   
    for (int count = 0; count < 6; count ++)
    {
        sumOfProbabilities[1] += changeDirectionFourNotes[count];
    }
    
    
    for (int count = 0; count < 12; count ++)
    {
        sumOfProbabilities[2] += changeDirectionFiveNotes[count];
    }
    
    threeNoteProbabilities.resize(sumOfProbabilities[0]);
    fourNoteProbabilities.resize(sumOfProbabilities[1]);
    fiveNoteProbabilities.resize(sumOfProbabilities[2]);
    
    chromaticVectorCounter = 0;
    
    for (int count = 0; count < 4; count ++)
    {
     
        for (int counter = 0; counter < changeDirectionThreeNotes[count] ; counter ++)
        {
            threeNoteProbabilities[chromaticVectorCounter] = count;
            chromaticVectorCounter ++;
        }
        
    }
    
    for (int count = 0; count < 6; count ++)
    {
        
        for (int counter = 0; counter < changeDirectionFourNotes[count] ; counter ++)
        {
            fourNoteProbabilities[chromaticVectorCounter] = count;
            chromaticVectorCounter ++;
        }
        
    }
    
    for (int count = 0; count < 12; count ++)
    {
        
        for (int counter = 0; counter < changeDirectionFiveNotes[count] ; counter ++)
        {
            fiveNoteProbabilities[chromaticVectorCounter] = count;
            chromaticVectorCounter ++;
        }
        
    }
    
    
    
    
}

void GenerateChromaticRun::calculateChromaticRun(int currentNote_, int direction_, bool tooHigh, bool tooLow , int randNum)
{
   
    
    int length = 0;
    int randomChromatic = 0;
    
   // randomChromatic = random.nextInt(sumOfChromaticCounters);
    
   // length = chromaticCounters[randomChromatic];
    
    int order  = 3;  // chose the length.
    
    int nextNote = 0;
    
    if (chromaticArray.size() != 0)
    {
        chromaticArray.clear();
    }
    
    chromaticArray.add(currentNote_);
    nextNote = currentNote_;
    
    
   // || randNum == 17|| randNum == 18|| randNum == 19|| || randNum == 43|| randNum == 44|| randNum == 70|| 
    
    if (randNum == 15 || randNum == 41) // if its two notes
    {
        if (randNum == 15 )
        {
            direction_ = -1;
        }
        
        else if(randNum == 41 )
        {
            direction_ = 1;
        }
        
        nextNote += direction_;
        
        chromaticArray.add(nextNote);
    }
    
    else if ( randNum == 42|| randNum == 16 || randNum == 56|| randNum == 57 || randNum == 19 || randNum == 70)
    {
        
        
        for (int count = 1; count < 3; count ++)
        {
       
            if (tooHigh == true)
            {
                order = 3;   // if its too high, decend
            }
            
            else if(tooLow == true)
            {
                order = 0;
            }
            
            else
            {
                
                if(randNum == 42)
                {
                    order = 0;
                }

               else if(randNum == 57)
                {
                    order = 1;
                }
               
                
               else if(randNum == 56)
               {
                   order = 2;
               }
              
                
               else if(randNum == 16)
               {
                   order = 3;
               }
            }
            
            
            
            nextNote += orderOfNotesThree[order][count];
            chromaticArray.add(nextNote);
            
        }
        
        
    }
    
    else if (length == 4)
    {
        if (tooHigh == true)
        {
            order = 5;   // if its too high, decend
        }
        
        else if(tooLow == true)
        {
            order = 0;
        }
        
        else
        {
            
            if (randNum == 70 )
            {
                order = 0;
            }
       
            else  if (randNum == 43)
                
            {
                
                order = 1;
            }
          
            
            else  if (randNum == 18)
                
            {
                
                order = 2;
            }
          
            
            else  if (randNum == 18)
                
            {
                
                order = 3;
            }
            else  if (randNum == 70)
                
            {
                
                order = 4;
            }
           else if (randNum == 19)
            {
                order = 5;
            }
       
        }
        
        for (int count = 1; count < 4; count ++)
        {
            nextNote += orderOfNotesFour[order][count];
            chromaticArray.add(nextNote);
            //std::cout << chromaticArray[count] << std::endl;
        }
        
        
    }
    else if (length == 5)
    {
        if(sumOfProbabilities[2] == 0)
        {
            
            randomChromatic = 1;
            order = 1;
        }
        
        else if (tooHigh == true)
        {
            order = 11;   // if its too high, decend
        }
        
        else if( tooLow == true)
        {
            order = 0;
        }
        
        else
        {
            randomChromatic = random.nextInt(sumOfProbabilities[0]);
            order = fiveNoteProbabilities[randomChromatic];
        }
       
        
        for (int count = 1; count < 5; count ++)
        {
            nextNote += orderOfNotesFive[order][count];
            chromaticArray.add(nextNote);
        }

    }
    

    lastNote = chromaticArray[chromaticArray.size() - 1];

    
}
