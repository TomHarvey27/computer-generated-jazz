/*
 ==============================================================================
 
 GenerateTriad.h
 Created: 21 Dec 2016 12:52:03pm
 Author:  Thomas Harvey
 
 ==============================================================================
 */

#include "../JuceLibraryCode/JuceHeader.h"

#ifndef GENERATETRIAD_H_INCLUDED
#define GENERATETRIAD_H_INCLUDED

class GenerateTriad
{
    
    GenerateTriad();
    ~GenerateTriad();
    
    void printTheTriadToNoteArray();
    
    
    
    
    int triad[7][3] =
    
    {{0,4,7},
        {2,5,9},
        {4,7,11},
        {5,9,0},
        {7,11,2},
        {9,0,4},
        {11,2,5}};
    
    
    int selectTriadBasedOnChord[7][3]=
    
    {{0,2,5},   // 1st, 3rd, 6th
        {1,3,6},
        {2,4,0},
        {3,5,1},
        {4,6,2},
        {5,0,3},
        {6,1,4}};
    
    
    
    int triadCount = 0;
    int octave = 0;
    int startingNote = 60;
    bool noteIsOctave = false;
    int sequenceCounter = 0;
    Array <int> notes;
    
    int whatTriad = 0;
    
    
    
};



#endif  // GENERATETRIAD_H_INCLUDED
