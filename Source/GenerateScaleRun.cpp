/*
 ==============================================================================
 
 GenerateScaleRun.cpp
 Created: 21 Dec 2016 12:52:12pm
 Author:  Thomas Harvey
 
 ==============================================================================
 */

#include "GenerateScaleRun.h"

GenerateScaleRun::GenerateScaleRun()
{
    
    
}
GenerateScaleRun::~GenerateScaleRun()
{
    
    
}


void GenerateScaleRun::setScaleRunProbabilities(int scaleRunCounter[6], int changeDirectionThreeNotes[4], int changeDirectionFourNotes[6] , int changeDirectionFiveNotes[12])
{
    
    int scaleRunVectorCounter = 0;
    
    for (int count = 0; count < 6; count ++)
    {
        sumOfScaleRunCounters += scaleRunCounter[count];
    }
    
    
   scaleRunCounters.resize(sumOfScaleRunCounters);
    
    for (int count = 0; count < 6; count ++)
    {
        
        for (int counter = 0; counter < scaleRunCounter[count] ; counter ++)
        {
             scaleRunCounters[scaleRunVectorCounter] = count;
            scaleRunVectorCounter ++;
        }
        
    }
    
    
    
    for (int count = 0; count < 4; count ++)
    {
        sumOfProbabilities[0] += changeDirectionThreeNotes[count];
    }
    
    
    for (int count = 0; count < 6; count ++)
    {
        sumOfProbabilities[1] += changeDirectionFourNotes[count];
    }
    
    
    for (int count = 0; count < 12; count ++)
    {
        sumOfProbabilities[2] += changeDirectionFiveNotes[count];
    }
    
    threeNoteProbabilities.resize(sumOfProbabilities[0]);
    fourNoteProbabilities.resize(sumOfProbabilities[1]);
    fiveNoteProbabilities.resize(sumOfProbabilities[2]);
    
    scaleRunVectorCounter = 0;
    
    for (int count = 0; count < 4; count ++)
    {
        
        for (int counter = 0; counter < changeDirectionThreeNotes[count] ; counter ++)
        {
            threeNoteProbabilities[scaleRunVectorCounter] = count;
            scaleRunVectorCounter ++;
        }
        
    }
    
    for (int count = 0; count < 6; count ++)
    {
        
        for (int counter = 0; counter < changeDirectionFourNotes[count] ; counter ++)
        {
            fourNoteProbabilities[scaleRunVectorCounter] = count;
            scaleRunVectorCounter ++;
        }
        
    }
    
    for (int count = 0; count < 12; count ++)
    {
        
        for (int counter = 0; counter < changeDirectionFiveNotes[count] ; counter ++)
        {
            fiveNoteProbabilities[scaleRunVectorCounter] = count;
            scaleRunVectorCounter ++;
        }
        
    }
  
    
}

void GenerateScaleRun::calculateScaleRun(int currentNote_ , int direction_ , bool tooHigh, bool tooLow, int randNum)
{
   
    int length = 0;
    int randomChromatic = 0;
    
    //randomChromatic = random.nextInt(sumOfScaleRunCounters);
    
  //  length = scaleRunCounters[randomChromatic];
   // std::cout << length << std::endl;
    
    int diatonicNotePosition = 0;
    int runOrder = 0;
    int noteIncrement = 0;
    int arrayIndex = 0;
    
    
    scaleRunArray.add(currentNote_); // add the first note to the array
    
    int currentNote = currentNote_;
    
    for (int count = 0; count < 7; count ++)
    {
        
        if ((currentNote_ - diatonicNotes[count] ) % 12 == 0)
        {
            diatonicNotePosition = count;
        }
        
    }
    
    
    
    
    if (randNum == 4 || randNum == 30)
    {
        
        if (tooHigh == true)
        {
            direction_ = -1;
        }
        
       else if (tooLow == true)
        {
            direction_ = 1;
        }
        
        arrayIndex = diatonicNotePosition + direction_;
        
        if (arrayIndex == 7)
        {
            diatonicNotePosition = 0 ;
        }
        
        
        noteIncrement = diatonicNotes[diatonicNotePosition + direction_] - diatonicNotes[diatonicNotePosition];
        

        if (diatonicNotePosition + direction_ < 0)
        {
            noteIncrement = diatonicNotes[diatonicNotePosition + direction_ + 7] - (diatonicNotes[diatonicNotePosition] + 12);
        }
        
        currentNote += noteIncrement;
        scaleRunArray.add(currentNote);
        
        length = 2;
        
    }
    
    
    else if (randNum == 5 || randNum == 31 || randNum == 54 || randNum == 55)
    {
    
        
        if (tooHigh == true)
        {
            runOrder = 3;   // if its too high, decend
        }
        
        else if( tooLow == true)
        {
            runOrder = 0;
        }
  
        
        else
            
        {
            if (randNum == 5)
            {
                runOrder = 3;
            }
            
            else  if (randNum == 31)
            {
                runOrder = 0;
            }
            
            else  if (randNum == 54)
            {
                runOrder = 2;
            }
            
            else  if (randNum == 55)
            {
                runOrder = 1;
            }
            
        }
        
        for (int count = 1; count < 3 ; count ++)
        {
            
            
            
            arrayIndex = diatonicNotePosition + orderOfNotesThree[runOrder][count];
            
            if (arrayIndex == 7)
            {
                diatonicNotePosition = 0 ;
            }
            
            
            
            noteIncrement = diatonicNotes[diatonicNotePosition + orderOfNotesThree[runOrder][count]] - diatonicNotes[diatonicNotePosition];
            //std::cout << "DN1 = " << diatonicNotes[diatonicNotePosition + orderOfNotesThree[runOrder][count]] << "DN2 = "<< diatonicNotes[diatonicNotePosition] << std::endl;
 
            
            if (diatonicNotePosition + orderOfNotesThree[runOrder][count] < 0)
            {
                noteIncrement = diatonicNotes[diatonicNotePosition + orderOfNotesThree[runOrder][count] + 7] -
                (diatonicNotes[diatonicNotePosition] + 12);
                diatonicNotePosition += orderOfNotesFour[runOrder][count] + 7;
            }
            
            currentNote += noteIncrement;
            scaleRunArray.add(currentNote);
            //std::cout << scaleRunArray[count] << std::endl;
            diatonicNotePosition += orderOfNotesThree[runOrder][count];
        }
        
        length = 3; // assigns the lenth of notes so the array is the correct size
    }
    
    
    
    
    else if (randNum == 6 || randNum == 7 || randNum == 8 || randNum == 32 || randNum == 33 || randNum == 78)
    {
        
        if (tooHigh == true)
        {
            runOrder = 5;   // if its too high, decend
        }
        
        else if( tooLow == true)
        {
            runOrder = 0;
        }
        
        
        else
            
        {
            if (randNum == 6)
            {
                runOrder = 3;
            }
            
            else  if (randNum == 7)
            {
                runOrder = 1;
            }
            
            else  if (randNum == 8)
            {
                runOrder = 5;
            }
            
            else  if (randNum == 32)
            {
                runOrder = 4;
            }
            
            else  if (randNum == 33)
            {
                runOrder = 2;
            }
            
            else  if (randNum == 78)
            {
                runOrder = 0;
            }
            
            
        }
        
        for (int count = 1; count < 4 ; count ++)
        {
            
             arrayIndex = diatonicNotePosition + orderOfNotesFour[runOrder][count];
            
            if (arrayIndex == 7)
            {
                diatonicNotePosition = 0 ;
            }
            
            noteIncrement = diatonicNotes[diatonicNotePosition + orderOfNotesFour[runOrder][count]] - diatonicNotes[diatonicNotePosition];
           
          
          
            
            if (diatonicNotePosition + orderOfNotesFour[runOrder][count] < 0)
            {
                noteIncrement = diatonicNotes[diatonicNotePosition + orderOfNotesFour[runOrder][count] + 7] -
                (diatonicNotes[diatonicNotePosition] + 12);
                diatonicNotePosition += orderOfNotesFour[runOrder][count] + 7;
            }
            
            currentNote += noteIncrement;
            scaleRunArray.add(currentNote);
            //std::cout << scaleRunArray[count] << std::endl;
            diatonicNotePosition += orderOfNotesFour[runOrder][count];
            
            
        }
        
        length = 4; // assigns the lenth of notes so the array is the correct size
    }
    
    else if (randNum == 34 || randNum == 9 )  // five notes
    {
        if (tooHigh == true)
        {
            runOrder = 11;   // if its too high, decend
        }
        
        else if( tooLow == true)
        {
            runOrder = 0;
        }
        
 
        
        for (int count = 1; count < 5 ; count ++)
        {
            
            
            arrayIndex = diatonicNotePosition + orderOfNotesFive[runOrder][count];
            
            if (arrayIndex == 7)
            {
                diatonicNotePosition = 0 ;
            }
            
            noteIncrement = diatonicNotes[diatonicNotePosition + orderOfNotesFive[runOrder][count]] - diatonicNotes[diatonicNotePosition];
   
            
            if (diatonicNotePosition + orderOfNotesFour[runOrder][count] < 0)
            {
                noteIncrement = diatonicNotes[diatonicNotePosition + orderOfNotesFive[runOrder][count] + 7] -
                (diatonicNotes[diatonicNotePosition] + 12);
                diatonicNotePosition += orderOfNotesFive[runOrder][count] + 7;
            }
            
            currentNote += noteIncrement;
            scaleRunArray.add(currentNote);
            diatonicNotePosition += orderOfNotesFive[runOrder][count];
            
        }
        
        length = 5; // assigns the lenth of notes so the array is the correct size
    }
    

    lastNote = scaleRunArray[scaleRunArray.size() -1];
    

    // USE A POINTER TO A Pointer to a 2D ARRAY SO THERES ONLY ONE LOOP, AND IF STATEMENTS SET WHICH ARRAY ITS POITING TO.
    
    
//    std::cout << length << std::endl;
//    int* returnScaleRun = new int[length];
//    
//    
//    for (int count = 1; count < scaleRunArray.size() + 1; count ++)
//    {
//        returnScaleRun[count] = scaleRunArray[count -1];
//        lastNote = scaleRunArray[count -1];
//       // std::cout << "scale run = " << scaleRunArray[count] << std::endl;
//        // std::cout << returnScaleRun[count] << std::endl;
//    }
    
   // returnScaleRun[0] = scaleRunArray.size(); // this might not need to be done, make a new type of outside of this, the size of the returned pointer
    
    //scaleRunArray.clear();
    
    
}
