/*
 ==============================================================================
 
 GenerateScaleRun.h
 Created: 21 Dec 2016 12:52:12pm
 Author:  Thomas Harvey
 
 ==============================================================================
 */

#include "../JuceLibraryCode/JuceHeader.h"
#include <vector>
using std::vector;

#ifndef GENERATESCALERUN_H_INCLUDED
#define GENERATESCALERUN_H_INCLUDED

class GenerateScaleRun
{
public:
    
    GenerateScaleRun();
    ~GenerateScaleRun();
    
    void calculateScaleRun(int currentNote_ ,int dicretion_, bool tooHigh, bool tooLow, int randNum);
    void setScaleRunProbabilities(int scaleRunCounter[6], int changeDirectionThreeNotes[4], int changeDirectionFourNotes[6] , int changeDirectionFiveNotes[12]);
    
    
    int lastNote = 0;
    int diatonicNotes[7] = {0 ,2 ,4 ,5 ,7 ,9 , 11};
    Array <int> scaleRunArray;
    
private:
    
    Random random;
    
    // MAKE THESE ORDERS IN A SEPERATE CLASS, SO THEY CAN BE USED ACROSS CLASSES
    
    int orderOfNotesThree[4][3] =
    
    {{0,1,1},
        {0,1,-1},
        {0,-1,1},
        {0,-1,-1}};
    
    // potentially add in different orders
    int orderOfNotesFour[6][4] =
    
    {{0,1,1, 1},
        {0,1,1,-1},
        {0,1,-1,-1},
        {0,-1,1,1},
        {0,-1,-1,1},
        {0,-1,-1,-1}};
    
    int orderOfNotesFive[12][5] =
    
    {{0,1,1, 1,1},
        {0,1,1,1,-1},
        {0,1,1,-1,1},
        {0,1,1,-1,-1},
        {0,1,-1,-1,1},
        {0,1,-1,-1,-1},
        {0,-1,1,1,1},
        {0,-1,1,1,1},
        {0,-1,-1,1,1},
        {0,-1,-1,-1,1},
        {0,-1,-1,1,-1},
        {0,-1,-1,-1,-1}};
    
    vector <int> scaleRunCounters;
    int sumOfScaleRunCounters = 0;
    int SUM = 0;
    
    vector <int> threeNoteProbabilities;
    vector <int> fourNoteProbabilities;
    vector <int> fiveNoteProbabilities;
    
    int sumOfProbabilities[3] = {0};
    
    int sumOfChromaticCounters = 0;
    
};



#endif  // GENERATESCALERUN_H_INCLUDED
